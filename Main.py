from SeqParser import SeqParser
from BlosumMatrix import BlosumMatrix


path = input()
X = int(input())
seq_set = ['A', 'C', 'G', 'T']

seq_parser = SeqParser(path, ".txt", " ")

blosum = BlosumMatrix(seq_set, seq_parser.seq_list, seq_parser.max_len, X)

for str_ in blosum.matrix:
    print(str_)
