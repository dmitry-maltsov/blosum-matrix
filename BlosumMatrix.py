import math
import itertools


class BlosumMatrix:

    def __init__(self, seq_set, seq_list, max_len, x):

        self.seq_set = seq_set
        self.seq_list = seq_list
        self.seq_len = max_len
        self.x = x

        # generating all nuc pairs
        nuc_comb = BlosumMatrix._get_nuc_comb(self.seq_set)

        # generating rows from the sequences
        self.nuc_rows_list = BlosumMatrix._get_nuc_rows_list(self.seq_list, self.seq_len)

        # then filtering them by input X
        self.nuc_rows_list = BlosumMatrix.filter_nuc_rows(self.nuc_rows_list, len(self.seq_list), x, self.seq_set)

        # counting the number of pairs for all rows
        self.T = BlosumMatrix.get_pairs_count_sum(self.nuc_rows_list, nuc_comb)

        # generating blosum matrix
        self.matrix = BlosumMatrix.fill_matrix(self.seq_set, self.nuc_rows_list, self.T)

    @staticmethod
    def _get_nuc_comb(seq_set):
        """
        Generates all pairs of sequence.

        Args:
            seq_set: sequence, may be list, set, tuple or string.

        Returns:
            A list of all pairs of the seqSet.
        """
        return list(itertools.combinations_with_replacement(set(seq_set), 2))

    @staticmethod
    def _get_nuc_rows_list(seq_list, seq_len):
        """
        Generates rows from sequences list.

        Args:
            seq_list: a list of sequences with same length.
            seq_len: sequences length.

        Returns:
            A list of rows.
        """
        nuc_rows_list = list()

        for nuc_ind in range(seq_len):
            nuc_row = list()
            for seq in seq_list:
                nuc_row.append(seq[nuc_ind])
            nuc_rows_list.append(nuc_row)
        return nuc_rows_list

    @staticmethod
    def _check_row(nuc_row, row_len, x, seq_set):
        """
        Checks if row passes a predetermined similarity threshold.

        Args:
            nuc_row: char list.
            row_len: row length.
            x: integer value, predetermined similarity threshold in percents.
            seq_set: list of all symbols of which the sequence consists.

        Returns:
            True if passes, False if not.
        """
        for nuc in seq_set:
            if (nuc_row.count(nuc) / row_len) * 100 >= x:
                return True
        return False

    @staticmethod
    def filter_nuc_rows(nuc_rows_list, row_len, x, seq_set):
        """
        Checks every row if it passes a predetermined similarity threshold.

        Args:
            nuc_rows_list: rows list.
            row_len: every row length.
            x: integer value, predetermined similarity threshold in percents.
            seq_set: list of all symbols of which the sequence consists.

        Returns:
            A list of rows which passes a similarity threshold.
        """
        filtered_nuc_rows = list()

        for nuc_row in nuc_rows_list:
            if BlosumMatrix._check_row(nuc_row, row_len, x, seq_set):
                filtered_nuc_rows.append(nuc_row)
        return filtered_nuc_rows

    @staticmethod
    def _count_nuc_pairs(nuc_row, nuc_comb):
        """
        Count the number of symbol pairs for a row.

        Args:
            nuc_row: char list.
            nuc_comb: all symbol pairs.

        Returns:
            Number of pairs in a row.
        """
        pairs_count = 0

        for first_nuc, last_nuc in nuc_comb:
            if first_nuc == last_nuc:
                nuc_numb = nuc_row.count(first_nuc)
                pairs_count += nuc_numb * (nuc_numb-1) / 2
            else:
                pairs_count += nuc_row.count(first_nuc)*nuc_row.count(last_nuc)
        return pairs_count

    @staticmethod
    def get_pairs_count_sum(nuc_rows_list, nuc_comb):
        """
        Count the number of symbol pairs of all rows.

        Args:
            nuc_rows_list: rows list.
            nuc_comb: all symbol pairs.

        Returns:
            Number of pairs of all rows.
        """
        pairs_count_sum = 0
        for nuc_row in nuc_rows_list:
            pairs_count_sum += BlosumMatrix._count_nuc_pairs(nuc_row, nuc_comb)
        return pairs_count_sum

    @staticmethod
    def get_mut_prob(nuc_rows_list, pair, period):
        """
        Calculates mutable probability for a single pair.

        Args:
            nuc_rows_list: rows list.
            pair: symbol pair.
            period: number of pairs for all rows.

        Returns:
            Mutable probability for a single pair.
        """
        pair_numb = 0

        for nuc_row in nuc_rows_list:
            pair_numb += BlosumMatrix._count_nuc_pairs(nuc_row, [pair])
        return pair_numb / period

    @staticmethod
    def get_symbol_prob(seq_set, symbol, nuc_rows_list, period):
        """
        Calculates probability of one symbol appearance.

        Args:
            seq_set: list of all symbols of which the sequence consists.
            symbol: symbol.
            nuc_rows_list: rows list.
            period: number of pairs for all rows.

        Returns:
            Probability of one symbol appearance.
        """
        dif_symbol_sum = 0
        for set_elem in seq_set:
            if set_elem != symbol:
                dif_symbol_sum += BlosumMatrix.get_mut_prob(nuc_rows_list, (set_elem, symbol), period)
        return BlosumMatrix.get_mut_prob(nuc_rows_list, (symbol, symbol), period) + dif_symbol_sum/2

    @staticmethod
    def get_expect_freq(symbol_prob_i, symbol_prob_j):
        """
        Calculates expected frequency for symbol pair.

        Args:
            symbol_prob_i: probability of first symbol appearance.
            symbol_prob_j: probability of second symbol appearance.

        Returns:
            Expected frequency.
        """
        if symbol_prob_i == symbol_prob_j:
            return symbol_prob_i * symbol_prob_j
        return symbol_prob_i * symbol_prob_j * 2

    @staticmethod
    def _get_blosum_elem(mut_prob, expect_freq):
        """
        Calculates elem for blosum matrix for symbol pair.

        Args:
            mut_prob: mutable probability of the pair.
            expect_freq: expected frequency of the pair.

        Returns:
            blosum matrix elem.
        """
        return 2 * math.log2(mut_prob / expect_freq)

    @staticmethod
    def fill_matrix(seq_set, nuc_rows_list, period):
        """
        Generates blosum matrix.

        Args:
            seq_set: list of all symbols of which the sequence consists.
            nuc_rows_list: rows list.
            period: number of pairs for all rows.

        Returns:
            blosum matrix.
        """
        set_len = len(seq_set)
        blosum = [[0 for _ in range(set_len)] for _ in range(set_len)]

        for elem_ind_i, seq_elem_i in enumerate(seq_set):
            for elem_ind_j, seq_elem_j in enumerate(seq_set):
                blosum[elem_ind_i][elem_ind_j] = BlosumMatrix._get_blosum_elem(
                    BlosumMatrix.get_mut_prob(nuc_rows_list, (seq_elem_i, seq_elem_j), period),
                    BlosumMatrix.get_expect_freq(BlosumMatrix.get_symbol_prob(seq_set, seq_elem_i, nuc_rows_list, period),
                                                 BlosumMatrix.get_symbol_prob(seq_set, seq_elem_j, nuc_rows_list, period)))
        return blosum


