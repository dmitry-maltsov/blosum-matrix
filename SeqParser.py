import os


class SeqParser:

    def __init__(self, path, exp, sep):

        # searching all file names with sequences and printing them
        self._files_list = SeqParser._find_files(path, exp)

        # parsing sequences from the files
        self.seq_list = SeqParser._parse_seq(self._files_list, sep)

        # making them all one length
        self.seq_list, self.max_len = SeqParser._equal_seq(self.seq_list)

    @staticmethod
    def _find_files(path, exp):
        """
        Finds all files with sequences at the given path.

        Args:
            path: full name of directory with files.

        Returns:
            A list of full file names containing sequences.
        """
        files_list = list()

        for root, dirs, files in os.walk(path):
            for file_ in files:
                if file_.endswith(exp):
                    files_list.append(root + "/" + file_)
        return files_list

    @staticmethod
    def _parse_seq(files_list, sep):
        """
        Parses file to get sequences.

        Args:
            files_list: a list with file names with sequences.

        Returns:
            A list of all sequences.
        """
        seq_list = list()

        for filename in files_list:
            with open(filename, 'r') as f:
                lines = f.readlines()
                for line in lines:
                    line_list = line.split(sep)
                    seq = line_list[-2]
                    seq_list.append(seq)
        return seq_list

    @staticmethod
    def _equal_seq(seq_list):
        """
        Makes all sequences same length by adding '-' at the end if necessary.

        Args:
            seq_list: a list of sequences with different lengths.

        Returns:
            A tuple which contains a list of sequences with same length = max(all lengths), max length.
        """
        max_len = max(map(len, seq_list))

        for seqIndex, seq in enumerate(seq_list):
            for i in range(max_len - len(seq)):
                seq += "-"
                seq_list[seqIndex] = seq
        return seq_list, max_len
